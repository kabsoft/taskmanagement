<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/', 'ProjectsController@index');
route::get('/documentation', 'ProjectsController@documentation');
route::get('/projects', 'ProjectsController@index');
route::get('/projects/create', 'ProjectsController@create');
route::get('/projects/{project}', 'ProjectsController@show');
route::get('/projects/{project}/edit', 'ProjectsController@edit');
route::Post('/projects', 'ProjectsController@store');
route::patch('/projects/{project}', 'ProjectsController@update');
route::delete('/projects/{project}', 'ProjectsController@destroy');
//Route::resource('projects', 'ProjectsController');

route::get('/tasks/{task}', 'ProjectTasksController@show');
route::get('/tasks/{task}/edit', 'ProjectTasksController@edit');
route::get('/tasks/{task}/deletetask', 'ProjectTasksController@deleteTask');
route::patch('/tasks/{task}/taskupdate', 'ProjectTasksController@taskupdate');
route::patch('/tasks/{task}', 'ProjectTasksController@update');
route::post('/tasks', 'ProjectTasksController@store');
route::delete('/tasks/{task}', 'ProjectTasksController@destroy');