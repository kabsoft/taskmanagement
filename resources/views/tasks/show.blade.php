@extends('layoutBulma')

@section('content')
<div class="panel">
    <div class="panel">
        <label class="label">Title:</label>
        <p>{{ $task->title }}</p>
    </div>
    <div class="panel">
        <label class="label"> Description: </label>
        <div> {{ $task->description }} </div>
    </div>
</div>


<div class="panel">
    <a href="/tasks/{{$task->id}}/edit" class="button is-link" role="button"> Edit task </a>
</div>
<!-- Delete task -->
<div class="panel">
    <form method="Post" action="/tasks/{{$task->id}}" id="deleteTaskFrm">
        @csrf
        @method('delete')
        <a href="#" class="button is-danger deletetaskbtn" role="button" onclick="deleteTask('{{$task->title}}' )"> Delete task </a>
    </form>
</div>
@endsection
