@extends('layoutBulma')

@section('content')
<h1 class="title"> Edit Task </h1>
<div class="panel">
    <form method="Post" action="/tasks/{{ $task->id}}/taskupdate">
        @csrf
        @method('patch')
        <div class="panel">
            <label class="label" for="title"> Title </label>
            <div>
                <input type="text" name='title' class="input {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ $task->title }}" required>
            </div>
        </div>
        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{ $task->description}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Update task</button>
        </div>
    </form>
</div>

@if ($errors->any())
<div class="notification is-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@endsection

