@extends('layoutBulma')

@section('content')



<div class="content">   
    <h1 class="title is-3"> Task management </h1>  
    <p class="title is-5">Back end</p>
    <ul class="menu-list">
        <li><a>PHP 7.1.16 </a></li>
        <li><a>MySql: 5.7.21</a></li>
        <li><a>Laravel Framework: 5.8.10 </</li> 
        <li><a>Eloquent ORM </a></li>
        <li><a>RestAPI </a></li>
    </ul>

    <p class="title is-5">Front end</p>
    <ul class="menu-list">
        <li><a>Bulma : CSS Framework </a></li>
        <li><a>jQuery</a></li>        
    </ul>

    <p class="title is-5">Tools</p>
    <ul class="menu-list">
        <li><a> Php artisan </a></li>
        <li><a>Composer</a></li>  
        <li><a>Git</a></li>        
    </ul>

    <div class="panel">
        <p class="title is-5">Files</p>
        <div class="panel">
            <div class="panel-block"> Controller: </div>
            <div class="panel-block">   app/Http/Controllers/ProjectTasksController.php </div>
            <div class="panel-block">  app/Http/Controllers/ProjectsController.php </div>
        </div>

        <div class="panel">
            <div class="panel-block"> View: </div>
            <div class="panel-block">   resources/views/projects/create.blade.php </div>
            <div class="panel-block">  resources/views/projects/edit.blade.php </div>
            <div class="panel-block">   resources/views/projects/index.blade.php </div>
            <div class="panel-block">  resources/views/tasks/edit.blade.php </div>
        </div>

        <div class="panel">
            <div class="panel-block"> Model: </div>
            <div class="panel-block">   app/Project.php </div>
            <div class="panel-block">  app/Task.php </div>
        </div>
        
         <div class="panel">
            <div class="panel-block"> Routes: </div>
            <div class="panel-block">   routes/web.php </div>           
        </div>

    </div>





</div>

@endsection
