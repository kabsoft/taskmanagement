@extends('layoutBulma')

@section('content')
<!-- h1> Index page </h1 -->
@if($projects->count())
<div class="panel">
    <p class="panel-heading">Projects:</p>
    <ul>
        @foreach($projects as $project)
        <li>        
            <a class="panel-block" href="/projects/{{ $project->id }}"> {{ $project->title }} </a>
        </li>   
        @endforeach
    </ul>
</div>
@endif
@endsection