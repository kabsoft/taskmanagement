@extends('layoutBulma')

@section('content')
<h1 class="title"> Create project</h1>
<div>
    <form method="Post" action="/projects">
        @csrf
        <div class="panel">
            <label class="label" for="title"> Title </label>
            <div>
                <input type="text" name='title' class="input {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ old('title') }}" required>
            </div>
        </div>
        <div class="panel">
            <label class="label" for="description"> Description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" >{{old('description')}}</textarea>
            </div>
        </div>
        <div>
            <button type="submit" class="button is-link">Create new project</button>
        </div>
    </form>
</div>
@if ($errors->any())
<div class="notification is-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@endsection

