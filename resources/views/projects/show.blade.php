@extends('layoutBulma')

@section('content')
@if($project->count())
<div class="panel">
    <div class="panel">
        <label class="label">Project:</label>
        <p>{{ $project->title }}</p>
    </div>
    <div class="panel">
        <label class="label"> Description: </label>
        <div> {{ $project->description }} </div>
    </div>
</div>

<div class="panel columns">
    <div class="column is-one-fifth">
        <a href="/projects/{{ $project->id }}/edit" class="button is-link"> Edit Project </a>
    </div>
    <div class="column is-one-fifth">
        <form method="Post" action="/projects/{{$project->id}}">
            @csrf
            @method('delete')
            <button type="submit" class="button is-danger" onclick="return confirm('Are you sure to delete project' + ' {{ $project->title }} ?' );"> Delete Project </button>
        </form>  
    </div>
</div>

<hr>
<div class="panel">
    <button class="button is-link add_task_button" > Add task </button>    
</div>
<div class="panel add_task {{ $errors->any() ? ' display-block' : '' }}">
    <form method="Post" action="/tasks">
        @csrf

        <div>
            <label class="label" for="title"> Task title: </label>
            <div>
                <input type="text" name='title' class="input {{ $errors->has('title') ? 'is-danger': '' }}" value="{{ old('title') }}">
            </div>
        </div>
        <div>
            <label class="label" for="description"> Task description </label>
            <div>
                <textarea class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name='description'> {{old('description')}} </textarea>
            </div>
        </div>
        <div>
            <input type="hidden" name='project_id' value="{{ $project->id }}">
        </div>
        <div class="margintop10px">
            <button type="submit" class="button is-link">Create new Task </button>
        </div>
    </form>
</div>
<!-- validation -->
@if ($errors->any())
<div class="notification is-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!---------- tasks list ------------->

@if($project->tasks->count())
<div class="panel">
    @foreach($project->tasks as $task)
    <form method="Post" action="/tasks/{{ $task->id }}">
        <div>
            @csrf
            @method('patch')
        </div>
        <div class="columns">
            <div class="column is-one-third">
                <input type="checkbox" name="completed" class="checkbox" onclick="this.form.submit();" {{ $task->completed ? 'checked' : '' }}>    
                <a href="/tasks/{{$task->id}}" class=" {{ $task->completed ? 'is-completed' : '' }}"> {{ $task->title }} </a> 
            </div>
            <div class="column is-one-fifth">
                <a class="button is-small is-link" href="/tasks/{{$task->id}}/edit"> Edit </a> 
            </div>
            <div class="column is-one-fifth">
                <a href="/tasks/{{$task->id}}/deletetask" class="button is-small is-danger" onclick="return confirm('Are you sure to delete task' + ' {{ $task->title }} ?' );">Delete</a>
            </div>
        </div>
    </form>
    @endforeach
</div>
@endif
<!-----------End task --------------------->
@endif
@endsection

