<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectsController extends Controller {

    public function index() {
        return view('projects.index', ['projects' => Project::all()]);
    }

    public function show(Project $project) {
        return view('projects.show', ['project' => $project]);
    }

    public function edit(Project $project) {
        return view('projects.edit', ['project' => $project]);
    }

    public function store() {
        $att = $this->validationPro();
        Project::create($att);
        return redirect('/projects');
    }

    public function create() {
        return view('projects.create');
    }

    public function update(Project $project) {
        $att = $this->validationPro();
        $project->update($att);
        return redirect('/projects');
    }

    public function destroy(Project $project) {
        $project->delete();
        return redirect('/projects');
    }

    public function validationPro() {
        return request()->validate([
                    'title' => ['required', 'min:3'],
                    'description' => ['required', 'min:3']
        ]);
    }
    
    public function documentation(){
        return view('documentation');
    }

}
