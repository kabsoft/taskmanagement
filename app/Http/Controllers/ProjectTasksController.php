<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;

class ProjectTasksController extends Controller {

    public function edit(Task $task) {
        return view('tasks.edit', ['task' => $task]);
    }

    public function taskupdate(Task $task) {
        $this->validationPro();
        $task->update(request(['title', 'description', 'project_id']));
        return view('tasks.show', ['task' => $task]);
    }

    public function deleteTask(Task $task) {
        $this->destroy($task);
        return back();
    }

    public function show(Task $task) {
        return view('tasks.show', ['task' => $task]);
    }

    public function update(Task $task) {
        $task->update([
            "completed" => request()->has('completed')]);
        return back();
    }

    public function store() {
        $att = $this->validationPro();
        Task::create(request(['title', 'description', 'project_id']));
        return back();
    }

    public function destroy(Task $task) {
        $task->delete();
        return redirect('/projects/' . $task->project_id);
    }

    public function validationPro() {
        return request()->validate([
                    'title' => ['required', 'min:3'],
                    'description' => ['required', 'min:3']
        ]);
    }

}
