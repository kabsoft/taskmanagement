Taskmanagement system.


PHP 7.1.16
MySql: 5.7.21
Laravel Framework: 5.8.10 
Eloquent ORM
RestAPI

FrontEnd:
  Bulma : css framework
  jQuery

Tool: 
	php artisan
	Composer
	Git
---------------------------------
Create Edit Delete Project
Create edit Delete Task
If a Project delete corresponding tasks will be deleted as well
Validation for both client and server side

-------------------------------
localhost:8000/projects  //Will show all the projects

------------------- files to check -------------
Controller:
app/Http/Controllers/ProjectTasksController.php
app/Http/Controllers/ProjectsController.php

View:
resources/views/projects/create.blade.php
resources/views/projects/edit.blade.php
resources/views/projects/index.blade.php
resources/views/tasks/edit.blade.php

Model:
app/Project.php
app/Task.php

Routes:
routes/web.php



 